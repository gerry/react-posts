import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import { getAuth, signInWithEmailAndPassword, signOut } from 'firebase/auth';
import firebase from './firebase';

import { useStorageState } from 'react-storage-hooks';
import Header from "./components/Header";
import Posts from "./components/Posts";
import Post from "./components/Post";
import NotFound from './components/NotFound';
import PostForm from './components/PostForm';
import Message from './components/Message';
import Login from './components/Login';
import UserContext from './context/UserContext';

import "./App.css";



const App = (props) => {
  const [posts, setPosts] = useStorageState(localStorage, `state-posts`, []);
  const [user, setUser] = useStorageState(localStorage, "state-user", {});
  const [nextAvailableId, setNextAvailableId] = useState(4);
  const [message, setMessage] = useState(null);

  const addNewPost = (post) => {
    post.id = posts.length + 1;
    post.slug = getNewSlugFromTitle(post.title);
    setPosts([...posts, post]);
    setFlashMessage('saved');
  }

  const getNewSlugFromTitle = (title) => {
    return encodeURIComponent(
      title.toLowerCase().split(" ").join("-")
    );
  }

  const updatePost = (post) => {
    post.slug = getNewSlugFromTitle(post.title);
    const index = posts.findIndex((p) => p.id === post.id);
    let oldPosts, updatedPosts;
    if(index > -1){
      oldPosts = posts.slice(0, index).concat(posts.slice(index + 1));
      updatedPosts = [...oldPosts, post].sort((a, b) => a.id - b.id);
    }
    else{
      updatedPosts = [...posts, post].sort((a, b) => a.id - b.id);
      setNextAvailableId(nextAvailableId + 1);
    }
    setPosts(updatedPosts);
    setFlashMessage('updated');
  }

  const deletePost = (post) => {
    if(window.confirm("Delete this post?")){
      const updatedPosts = posts.filter((p) => p.id !== post.id);
      setPosts(updatedPosts);
      setFlashMessage(`deleted`);
    }
  }

  const setFlashMessage = (message) => {
    setMessage(message)
    setTimeout(() => {
      setMessage(null)
    }, 1600)
  }

  const onLogin = (email, password) => {
    const auth = getAuth(firebase);
    signInWithEmailAndPassword(auth, email, password)
      .then((response) => {
        setUser({
          email: response.user["email"],
          isAuthenticated: true,
        })
      })
      .catch(error => console.error(error));
  }

  const onLogout = () => {
    const auth = getAuth(firebase);
    signOut(auth)
      .then(() => {
        setUser({ isAuthenticated: false });
      })
      .catch((error) => console.error(error));
  }

  return (
    <Router>
      <UserContext.Provider value={{ user, onLogin, onLogout }}>
        <div className="App">
          <Header />
          {message && <Message type={message} />}
          <Switch>
            <Route
              exact
              path="/"
              render={() => <Posts posts={posts} deletePost={deletePost} />}
            />
            <Route
              path="/posts/:postSlug"
              render={(props) => {
                const post = posts.find((post) => post.slug === props.match.params.postSlug);
                if (post) return <Post post={post} />;
                else return <NotFound />;
              }
              }
            />
            <Route
              exact
              path="/new"
              render={() => (
                user.isAuthenticated ? 
                  <PostForm
                    addNewPost={addNewPost}
                    post={{ id: nextAvailableId, slug: "", title: "", content: "" }}
                    setFlashMessage={setFlashMessage} />
                : 
                  <Redirect to="/login" />
              )}
            />
            <Route
              path="/edit/:postSlug"
              render={(props) => {
                const post = posts.find((post) => post.slug === props.match.params.postSlug);
                if (post) {
                  if(user.isAuthenticated) {
                    return <PostForm post={post} updatePost={updatePost} />;
                  }
                  else {
                    return <Redirect to="/login" />
                  }
                }
                else {
                  return <Redirect to="/" />;
                }
              }}
            />
            <Route
              exact
              path="/login"
              render={() => 
                !user.isAuthenticated ? <Login /> : <Redirect to="/" />
              }
            />
            <Route component={NotFound} />
          </Switch>
        </div>
      </UserContext.Provider>
    </Router>
  )
}

export default App
