import { initializeApp } from "firebase/app";
import "firebase/auth";
import "firebase/database";

// Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB18BAD5zAabppD6oIDri-xnDbwnKazJ60",
  authDomain: "react-blog-demo-c45a5.firebaseapp.com",
  projectId: "react-blog-demo-c45a5",
  storageBucket: "react-blog-demo-c45a5.appspot.com",
  messagingSenderId: "233163603650",
  appId: "1:233163603650:web:d5df1699e3514d00e8855b",
  measurementId: "G-ZPMD767EZ9"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const analytics = getAnalytics(firebase);

export default firebase;